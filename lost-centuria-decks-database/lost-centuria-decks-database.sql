CREATE TYPE "card_quality" AS ENUM (
  'none',
  'normal',
  'rare',
  'hero',
  'legend'
);

CREATE TYPE "card_type" AS ENUM (
  'monster',
  'spell'
);

CREATE TABLE "cards" (
  "id" int PRIMARY KEY,
  "cardname" varchar,
  "subname" varchar,
  "abilityname" varchar,
  "description" varchar,
  "picture" uuid,
  "cost" int,
  "quality" card_quality,
  "type" card_type
);

CREATE TABLE "deck_cards" (
  "card_id" int,
  "deck_id" uuid
);

CREATE TABLE "decks" (
  "id" uuid PRIMARY KEY,
  "deckname" varchar,
  "description" varchar,
  "picture" uuid,
  "rate" int,
  "avg_cost" float8
);

ALTER TABLE "deck_cards" ADD FOREIGN KEY ("card_id") REFERENCES "cards" ("id");

ALTER TABLE "deck_cards" ADD FOREIGN KEY ("deck_id") REFERENCES "decks" ("id");
